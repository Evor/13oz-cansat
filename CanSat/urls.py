"""CanSat URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.static import serve as static_serve

from PL_13OZ import views as app_view
from . import settings

urlpatterns = [
    url(r'^$', app_view.main),
    url(r'^post/(?P<id>\d+)/$', app_view.page, name="detail"),
    url(r'^team/', app_view.team, name="team"),
    url(r'^member/(?P<id>\d+)/$', app_view.member, name="member"),
    url(r'^gifts/', app_view.gifts, name="gift"),

    url(r'^ckeditor/', include("ckeditor_uploader.urls")),
    url(r'^admin/', admin.site.urls),

    url(
        regex=r'^chart_data/$',
        view=app_view.charts_data,
        name='charts_data',
    ),

    url(r'^project/$', app_view.project, name="project"),
    url(r'^charts/(?P<chart>.*)$', app_view.charts),
    url(r'^maps/$', app_view.maps, name="map"),
    # MediaPath fix
    url(r'^media/(?P<path>.*)$', static_serve, {'document_root': settings.MEDIA_ROOT, 'show_indexes': False}),
    url(r'^static/(?P<path>.*)$', static_serve, {'document_root': settings.STATIC_ROOT, 'show_indexes': False}),

]

