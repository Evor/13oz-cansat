from .models import Sidebar, Sponsor


def show_sidebars(request):
    return {'sidebar_list': Sidebar.objects.all(), 'sponsor_list': Sponsor.objects.all(), }
