from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from . import models


class WpisAdmin(admin.ModelAdmin):
    list_display = ('sTitle', 'oAuthor', 'dCreated')

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'oAuthor', None) is None:
            obj.oAuthor = request.user
        obj.save()


class SidebarAdmin(admin.ModelAdmin):
    list_display = ('sTitle', )


class SponsorAdmin(admin.ModelAdmin):
    readonly_fields = ('image_tag',)


class TShirtAdmin(admin.ModelAdmin):
    readonly_fields = ('image_tag',)


class TeamInline(admin.StackedInline):
    model = models.TeamMember
    can_delete = False


class TeamAdmin(UserAdmin):
    inlines = (TeamInline, )


admin.site.unregister(User)
admin.site.register(User, TeamAdmin)
admin.site.register(models.Wpis, WpisAdmin)
admin.site.register(models.TShirt, TShirtAdmin)
admin.site.register(models.Sponsor, SponsorAdmin)
admin.site.register(models.GlobalSettings, admin.ModelAdmin)
admin.site.register(models.Sidebar, SidebarAdmin)
