# Create your views here.

# from chartjs.views.lines import BaseLineChartView
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import connection
from django.shortcuts import render, get_object_or_404, HttpResponse

# from django.views.generic import TemplateView

from .models import Wpis, TShirt, GlobalSettings
import json


def main(request):
    wpis_list_all = Wpis.objects.all()
    paginator = Paginator(wpis_list_all, 10)

    page = request.GET.get('page')
    try:
        wpis_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        wpis_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        wpis_list = paginator.page(paginator.num_pages)

    context = {
        "wpis_list": wpis_list,
        "range": range(1, wpis_list.paginator.num_pages + 1),
    }
    return render(request, "base.html", context)


def page(request, id=None):
    post = get_object_or_404(Wpis, id=id)
    context = {
        "obj": post,
    }
    return render(request, "page.html", context)


def team(request):
    context = {
        "member_list": User.objects.all(),
    }
    return render(request, "team.html", context)


def gifts(request):
    context = {
        "gift_list": TShirt.objects.all(),
    }
    return render(request, "gift.html", context)


def member(request, id=None):
    user = get_object_or_404(User, id=id)
    context = {
        "user": user,
    }
    return render(request, "member.html", context)


def charts(request, chart="height"):
    context = {"render_chart": chart}
    return render(request, "charts.html", context)


# Bardzo proste te klasy się robią :)
def project(request):
    context = {
        "o_nas": GlobalSettings.objects.get_setting("O_NAS"),
    }
    return render(request, "project.html", context)


def date_handler(obj):
    return obj.isoformat() if hasattr(obj, 'isoformat') else obj


def maps(request):
    c = connection.cursor()
    c.execute("select * from 13oz_main.cansat_gps order by id desc;")
    data = c.fetchall()
    lat_cur = 0
    lng_cur = 0

    if data:
        lat_cur = data[0][1]
        lng_cur = data[0][2]

    context = {
        "lat_cur": lat_cur,
        "lng_cur": lng_cur,
        "markers": data,
    }
    return render(request, "maps.html", context)


def charts_data(request):
    params = request.GET
    name = params.get('name', '')
    c = connection.cursor()
    c.execute("SELECT num, name, value, time FROM 13oz_main.cansat_data;")

    sorted_dict = {}
    y_line = ("h", "t", "p", "d" )
    chart_data = {}
    max_series = 0

    for key in y_line:
        sorted_dict[key] = {}
        chart_data[key] = {}
        chart_data[key]["y"] = []
        chart_data[key]["time"] = []
        chart_data[key]["error"] = []

    for val in c.fetchall():
        if not val[0] in sorted_dict[val[1]]:
            sorted_dict[val[1]][val[0]] = {}

        if val[1] == name[0] and val[0] > max_series:
            max_series = val[0]
        # Zaszumione dane niechaj beda pomijane, z SD mamy czyste
        try:
            sorted_dict[val[1]][val[0]]["y"] = float(val[2])
            sorted_dict[val[1]][val[0]]["time"] = val[3]
            sorted_dict[val[1]][val[0]]["error"] = [float(val[2]) - 1.0, float(val[2]) + 1.0]
        except:
            continue

    for key in y_line:
        for i in range(1, max_series + 1):
            if i not in sorted_dict[key]:
                continue
            try:
                chart_data[key]["y"].append(sorted_dict[key][i]["y"])
                chart_data[key]["time"].append(sorted_dict[key][i]["time"])
                chart_data[key]["error"].append(sorted_dict[key][i]["error"])
            except:
                continue

    c.execute("SELECT type FROM 13oz_main.chart_type WHERE name='%s';" % name[0])
    chart_data["chart_type"] = c.fetchone()[0]

    chart_data["y_line"] = y_line
    chart_data["x_line"] = name[0]

    chart_data["name_dict"] = {
        "h": "Wysokość (m)",
        "p": "Ciśnienie (mBa)",
        "d": "Wilgotność (%)",
        # "m": "Pole magnetyczne (mGa)",
        "t": "Temperatura (*C)",
    }

    return HttpResponse(json.dumps(chart_data, default=date_handler), content_type='application/json')

