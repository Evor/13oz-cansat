from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models


class DefQuerySet(models.QuerySet):
    def can_show(self):
        return True


class Wpis(models.Model):
    sTitle = models.CharField(max_length=256, verbose_name="Tytuł")
    sBody = RichTextUploadingField(verbose_name="Treść")
    # bPublished = models.BooleanField(default=True, verbose_name="Wyświetl na stronie głównej")
    dCreated = models.DateTimeField(auto_now_add=True, verbose_name="Utworzono")
    dModified = models.DateTimeField(auto_now=True)
    oAuthor = models.ForeignKey(User, verbose_name="Autor", null=True, blank=True)
    objects = DefQuerySet.as_manager()

    def __str__(self):
        return self.sTitle

    def __iter__(self):
        return [
            self.sTitle,
        ]

    def get_absolute_url(self):
        return reverse("detail", kwargs={"id": self.id})

    class Meta:
        verbose_name = "Artykuł"
        verbose_name_plural = "Artykuły"
        ordering = ["-dCreated"]


class SidebarQS(models.QuerySet):
    def can_show(self):
        return self.filter(bPublished=True)


class Sidebar(models.Model):
    sTitle = models.CharField(max_length=256, verbose_name="Tytuł")
    sBody = RichTextUploadingField(verbose_name="Treść")
    bPublished = models.BooleanField(default=True, verbose_name="Wyświetl na stronie głównej")
    objects = SidebarQS.as_manager()

    def __str__(self):
        return self.sTitle

    class Meta:
        verbose_name = "Ogłoszenie"
        verbose_name_plural = "Ogłoszenia"


class TeamMember(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    sBody = RichTextUploadingField(verbose_name="O mnie")


class Sponsor(models.Model):
    sName = models.CharField(max_length=256, verbose_name="Nazwa")
    sUrl = models.CharField(max_length=256, verbose_name="Adres domowy")
    iWidth = models.IntegerField(default=128, verbose_name="Szerokośc miniaturki")
    iHeight = models.IntegerField(default=128, verbose_name="Wysokość miniaturki")

    def image_tag(self):
        return u'<img width="%s" height="%s" src="%s" />' % (str(self.iWidth), str(self.iHeight), self.iImage.url)

    image_tag.short_description = 'Obrazek wyświetlany na stronie głównej'
    image_tag.allow_tags = True

    iImage = models.ImageField(verbose_name="Miniaturka")


class TShirt(models.Model):
    sName = models.CharField(max_length=256, verbose_name="Nazwa wzoru")
    oAuthor = models.ForeignKey(User, verbose_name="Autor", null=True, blank=True)
    objects = DefQuerySet.as_manager()

    def image_tag(self):
        return u'<img width="256" height="256" src="%s" />' % self.iImage.url

    image_tag.short_description = 'Obrazek wyświetlany na stronie głównej'
    image_tag.allow_tags = True

    iImage = models.ImageField(verbose_name="Miniaturka wzoru")


class GlobalSettingsManager(models.Manager):
    def get_setting(self, key):
        try:
            setting = GlobalSettings.objects.get(key=key)
        except:
            raise "Error"
        return setting


class GlobalSettings(models.Model):
    key = models.CharField(unique=True, max_length=255, verbose_name="Nazwa")
    value = RichTextUploadingField(verbose_name="Wartość")

    def __str__(self):
        return self.key

    class Meta:
        verbose_name = "Globalne"
        verbose_name_plural = "Globalne"

    objects = GlobalSettingsManager()
